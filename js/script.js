var timer;//Timer
$(document).ready(function() {
  //style the form elements
  if($(".form-container").length>0){
    for(var i=0;i<$('.form-container').length;i++)
    {
      if(!$('.form-container').eq(i).parent().parent().hasClass("sample-row"))
      {
        $('.form-container').eq(i).jqTransform({imgPath:'i/'});
      }
    }
  }
  
  //init calendar
  if($(".from-date").length>0)
  {
    $(".from-date").datepicker({
      dateFormat:"mm/dd/yy",
      showOtherMonths: true
    });
  }
  $(".date-icon,.to-date-icon").bind('click',function(){
    $(this).prev().trigger("click").focus();
  });
  if($(".to-date").length>0)
  {
    $(".to-date").datepicker({
      dateFormat:"mm/dd/yy",
      showOtherMonths: true
    });
  }

  //init the table sorter
  //Panel page
  $(".panel-contents #draft-table").tablesorter({
    headers:{0:{ sorter: false},4:{ sorter: false}},
    widgets        : ['zebra', 'columns'],
    usNumberFormat : false,
    sortRestart    : true
  });
  //Assign Access page
  $(".assign-access-contents #draft-table").tablesorter({
    widgets        : ['zebra', 'columns'],
    usNumberFormat : false,
    sortRestart    : true
  });
  //Change Access page
  $(".change-access-contents #draft-table").tablesorter({
    widgets        : ['zebra', 'columns'],
    usNumberFormat : false,
    sortRestart    : true
  });
  //Integrate Events page
  $(".integrate-events-contents #draft-table").tablesorter({
    headers:{0:{ sorter: false}},
    widgets        : ['zebra', 'columns'],
    usNumberFormat : false,
    sortRestart    : true
  });
  
  //style the custom scrollbar
  function renderScrollbar()
  {
    if($('.drop-down.filter-dropdown').length>0)
    {
      for(var i=0;i<$('.drop-down.filter-dropdown').length;i++)
      {
        $('.drop-down.filter-dropdown').eq(i).mCustomScrollbar("destroy");
        $('.drop-down.filter-dropdown').eq(i).mCustomScrollbar();
      }
    }
  }
  renderScrollbar();
  
  
  //click on page body to close the popups
  $("body").click(function(){
    //close user popup
    $(".user-name .user-txt").removeClass("active");
    $(".user-name .user-txt").next().addClass("hide");
    
    //close notification popup
    $(".icon-notification").removeClass("active");
    $(".icon-notification").next().addClass("hide");
    
    //close filter popup
    $(".filter-dropdown").addClass("hide");
  })
  
  //click on option of dropdown list to select the option
  $(".dropdown-menu.option-select li a").click(function(){
    var value = $(this).find("span.value").html();
    $(this).parents(".dropdown-menu.option-select").prev().find(".selected-option").html(value);
    
    $(this).parents(".dropdown-menu.option-select").find("li").removeClass("current");
    $(this).parent().addClass("current");
  })
  
  //click Login button
  $(".login-btn").click(function(){
    if($(this).hasClass("btn-gray"))
      return;
    
    $(".error-invalid").addClass("hide");
    $(".error-choose").addClass("hide");
    $(".error-failed").addClass("hide");
    
    if($(".choose-a-directory-select li.current .value").html() === "Choose a directory")
    {
      $(".header-panel").addClass("error-panel");
      $(".error-choose").removeClass("hide");
      return;
    }
    
    if(($(".username-input").val() === "user") && ($(".password-input").val() === "pass"))
    {
      location.href = "panel.html";
    }
    else
    {
      $(".error-invalid").removeClass("hide");
      $(".header-panel").addClass("error-panel");
    }
  })
  
  //click "icon-collapse-menu" button
  $(".left-aside .icon-collapse-menu").click(function(){
    if($(this).parents(".left-aside").hasClass("aside-icon"))
    {
      //expand the left side menu
      $(this).parents(".left-aside").removeClass("aside-icon");
      $(".wrapper").removeClass("left59");
    }
    else
    {
      //collapse the left side menu
      $(this).parents(".left-aside").addClass("aside-icon");
      $(".wrapper").addClass("left59");
    }
  })
  
  //click on Username/Notification to show User/Notification popup
  $(".user-name .user-txt,.icon-notification").click(function(event){
    if(!$(this).hasClass("active"))
    {
      //show User poup
      $("body").click();
      
      $(this).addClass("active");
      $(this).next().removeClass("hide");
    }
    else
    {
      //hide User popup
      $("body").click();
    }
    
    event.stopPropagation();
  })
  
  //click on Nitification popup
  $(".notification-dropdown").click(function(event){
    event.stopPropagation();
  })
  
  //click full-screen button to show full-screen mode
  $(".icon-full-screen").click(function(){
    if(!$(".wrapper").hasClass("wrapper-full-screen"))
    {
      //show full-screen mode
      $(".wrapper").addClass("wrapper-full-screen");
      $(".left-aside").addClass("hide");
    }
    else
    {
      //hide full-screen mode
      $(".wrapper").removeClass("wrapper-full-screen");
      $(".left-aside").removeClass("hide");
    }
  })
  
  //click Search Filter button to show Filter popup
  $(".icon-search-down").click(function(event){
    if($(this).parent().find(".filter-dropdown").hasClass("hide"))
    {
      $("body").click();
      $(this).parent().find(".filter-dropdown").removeClass("hide");
    }
    else
    {
      $("body").click();
    }
    
    event.stopPropagation();
  })
  
  //click on Filter popup
  $(".filter-dropdown").click(function(event){
    event.stopPropagation();
  })
  
  //click checkboxes in the Words section on Filter popup
  $(".filter-dropdown ul.word-list .jqTransformCheckbox").click(function(){
    var filter_list = $(this).parents(".filter-dropdown ul").find(".jqTransformCheckbox");
    var index = filter_list.index($(this));
    if($(this).hasClass("jqTransformChecked"))
    {
      //uncheck other checkboxes
      for(var i=0;i<filter_list.length;i++)
      {
        if(i === index)
        {
          continue;
        }
        filter_list.eq(i).removeClass("jqTransformChecked");
        filter_list.eq(i).next().prop("checked",false);
      }
    }
    else
    {
      $(this).addClass("jqTransformChecked");
      $(this).next().prop("checked","checked");
    }
  })
  
  //click checkboxes in the Columns section on Filter popup
  $(".filter-dropdown ul.column-list .jqTransformCheckbox").click(function(){
    var filter_list = $(this).parents(".filter-dropdown ul").find(".jqTransformCheckbox");
    var index = filter_list.index($(this));
    if($(this).hasClass("jqTransformChecked"))
    {
      if(index === 0)
      {
        //check All columns checkbox
        
        //uncheck other individual columns checkboxes
        for(var i=0;i<filter_list.length;i++)
        {
          if(i===0)
          {
            continue;
          }
          filter_list.eq(i).removeClass("jqTransformChecked");
          filter_list.eq(i).next().prop("checked",false);
        }
      }
      else
      {
        //check individual columns checkbox
        
        //uncheck All columns checkbox
        filter_list.eq(0).removeClass("jqTransformChecked");
        filter_list.eq(0).next().prop("checked",false);
      }
    }
    else
    {
      if($(this).parents(".filter-dropdown ul").find(".jqTransformCheckbox.jqTransformChecked").length === 0)
      {
        //at least on checkbox need to be checked
        $(this).addClass("jqTransformChecked");
        $(this).next().prop("checked","checked");
      }
    }
  })

  //enter in Search input
  $(".search-input").keyup(function(){
    if($(this).val() !== "")
    {
      $(this).parent().find(".icon-search-remove").removeClass("hide");
    }
    else
    {
      $(this).parent().find(".icon-search-remove").addClass("hide");
    }
  })
  
  //click X icon of the Search input
  $(".icon-search-remove").click(function(){
    $(this).parent().find(".search-input").val("");
    $(this).addClass("hide");
  })
  
  //click Add Panel button in Panel page
  $(".add-panel-btn").click(function(){
    if(!$(this).parents(".wrapper").find(".right-panel-add").hasClass("hide"))
      return;
      
    //show right side panel
    $(this).parents(".wrapper").find(".container-row").addClass("container-split");
    $(this).parents(".wrapper").find(".right-panel-add").removeClass("hide");
    $(this).parents(".wrapper").find(".right-panel-edit").addClass("hide");
    $(this).parents(".wrapper").find(".right-panel-add").find("input").val("");
    $(this).parents(".wrapper").find(".right-panel-add").find(".save-gray-btn").removeClass("hide");
    $(this).parents(".wrapper").find(".right-panel-add").find(".save-fail-btn").addClass("hide");
    $(this).parents(".wrapper").find(".right-panel-add").find(".save-success-btn").addClass("hide");
    
    //hide Import button and Action dropdown
    $(".actions-dropdown").addClass("hide");
    $(".import-panel-btn").addClass("hide");
    
    //show new row in table
    if(!$(".no-data-txt").hasClass("hide"))
    {
      $(".no-data-txt").addClass("hide");
    }
    var new_row = $("#draft-table .sample-row.hide").clone(true);
    
    new_row.removeClass("sample-row").removeClass("hide").addClass("temp-row");
    new_row.insertAfter($("#draft-table .sample-row.hide"));
    new_row.find('.form-container').jqTransform({imgPath:'i/'});
    
    //click checkbox of Table rows in Panel List page and Integrate Events page
    new_row.find(".jqTransformCheckboxWrapper .jqTransformCheckbox").click(function(){
      if($(this).hasClass("jqTransformChecked"))
      {
        if(!$(this).parents("#draft-table tr").hasClass("disabled-row"))
          $(this).parents("#draft-table tr").addClass("yellow-tr");
        else
          $(this).parents("#draft-table tr").removeClass("yellow-tr");
      }
      else
      {
        $(this).parents("#draft-table tr").removeClass("yellow-tr");
      }
      
      var table_rows = $(this).parents("#draft-table").find("tbody tr");
      if(table_rows.find("a.jqTransformCheckbox").length === table_rows.find("a.jqTransformCheckbox.jqTransformChecked").length)
      {
        //check check all checkbox
        $(this).parents("#draft-table").find("thead tr a.jqTransformCheckbox").addClass("jqTransformChecked");
      }
      else
      {
        //uncheck check all checkbox
        $(this).parents("#draft-table").find("thead tr a.jqTransformCheckbox").removeClass("jqTransformChecked");
      }
    })
    new_row.find(".jqTransformCheckboxWrapper").click(function(event){
      event.stopPropagation();
    })
 
    $(".clicked-tr").removeClass("clicked-tr");
    $("#draft-table .current-tr").removeClass("current-tr");
  })
  
  //click X icon in Add Panel section
  $(".right-panel-add .icon-cancel-btn").click(function(){
    $(this).parents(".wrapper").find(".container-row").removeClass("container-split");
    $(this).parents(".wrapper").find(".right-panel-add").addClass("hide");
    $(this).parents(".wrapper").find(".right-panel-edit").addClass("hide");
    
    $("#draft-table .temp-row").remove();
    $(".import-panel-btn").removeClass("hide");
    if(!$("#draft-table tbody").hasClass("empty-table"))
    {
      $(".actions-dropdown").removeClass("hide");
    }
    else
    {
      $(".no-data-txt").removeClass("hide");
    }
  })
  
  //click X icon and Save button in Edit Panel section
  $(".right-panel-edit .icon-cancel-btn,.right-panel-edit .save-panel-btn").click(function(){
    $(this).parents(".wrapper").find(".container-row").removeClass("container-split");
    $(this).parents(".wrapper").find(".right-panel-add").addClass("hide");
    $(this).parents(".wrapper").find(".right-panel-edit").addClass("hide");
    
    $(".actions-dropdown").removeClass("hide");
    $(".import-panel-btn").removeClass("hide");
    $("#draft-table .current-tr").removeClass("current-tr");
  })
  
  //enter text in Add Panel section
  $(".right-panel-add .right-panel-content input,.right-panel-edit .right-panel-content input").keyup(function(){
    var inputs = $(this).parents(".right-panel-content").find("input");
    var pass = true;
    for(var i=0;i<inputs.length;i++)
    {
      if(inputs.eq(i).val() === "")
      {
        pass = false;
      }
    }
    
    if(pass)
    {
      $(this).parents(".right-panel-content").prev().find(".save-gray-btn").addClass("hide");
      $(this).parents(".right-panel-content").prev().find(".save-fail-btn").removeClass("hide");
      $(this).parents(".right-panel-content").prev().find(".save-success-btn").addClass("hide");
    }
    else
    {
      $(this).parents(".right-panel-content").prev().find(".save-gray-btn").removeClass("hide");
      $(this).parents(".right-panel-content").prev().find(".save-fail-btn").addClass("hide");
      $(this).parents(".right-panel-content").prev().find(".save-success-btn").addClass("hide");
    }
  })
  
  //click Save button first time in Add Panel section
  $(".right-panel-add .save-fail-btn").click(function(){
    $(".right-panel-add .save-fail-btn").addClass("hide");
    $(".right-panel-add .save-success-btn").removeClass("hide");
  })
  
  //click Save button second time in Add Panel section
  $(".right-panel-add .save-success-btn").click(function(){
    $(".right-panel-add .save-fail-btn").removeClass("hide");
    $(".right-panel-add .save-success-btn").addClass("hide");
  })
  
  //click Done button in Add Panel modal window
  $("#modal-add-panel-connection-successful .done-add-panel-btn").click(function(){
    $(".wrapper").find(".container-row").removeClass("container-split");
    $(".wrapper").find(".right-panel-add").addClass("hide");
    $(".wrapper").find(".right-panel-edit").addClass("hide");
    
    //show Import button and Action dropdown
    $(".actions-dropdown").removeClass("hide");
    $(".import-panel-btn").removeClass("hide");
    
    if($("#draft-table tbody").hasClass("empty-table"))
    {
      //show whole table list
      $("#draft-table .temp-row").remove();
      
      $("#draft-table tbody").removeClass("empty-table");
      var table_rows = $("#draft-table tbody").find("tr");
      for(var i=0;i<table_rows.length;i++)
      {
        if(table_rows.eq(i).hasClass("sample-row"))
          continue;
          
        table_rows.eq(i).removeClass("hide");
      }
      
      $(".title-area .record-txt").html("13 of 999");
    }
    else
    {
      //add the new row
      var panel_name_text = $(".wrapper").find(".right-panel-add").find(".panel-name-input").val();
      var ip_address_text = $(".wrapper").find(".right-panel-add").find(".ip-address-input").val();
      var dns_name_text = $(".wrapper").find(".right-panel-add").find(".dns-name-input").val();
      $("#draft-table .temp-row").find("td").eq(1).html(panel_name_text);
      $("#draft-table .temp-row").find("td").eq(2).html(ip_address_text);
      $("#draft-table .temp-row").find("td").eq(3).find("span").html(dns_name_text);
      $("#draft-table .temp-row").removeClass("temp-row");
    }
  })
  
  //click Import button first time in Import Panels modal window
  $(".import-panels-import-fail-btn").click(function(){
    $(".import-panels-import-fail-btn").addClass("hide");
    $(".import-panels-import-success-btn").removeClass("hide");
  })
  
  //click Import button second time in Import Panels modal window
  $(".import-panels-import-success-btn").click(function(){
    $(".import-panels-import-fail-btn").removeClass("hide");
    $(".import-panels-import-success-btn").addClass("hide");
    
    if($("#draft-table tbody").hasClass("empty-table"))
    {
      //show whole table list
      $("#draft-table .temp-row").remove();
      
      $("#draft-table tbody").removeClass("empty-table");
      var table_rows = $("#draft-table tbody").find("tr");
      for(var i=0;i<table_rows.length;i++)
      {
        if(table_rows.eq(i).hasClass("sample-row"))
          continue;
          
        table_rows.eq(i).removeClass("hide");
      }
      
      $(".title-area .record-txt").html("13 of 999");
      
      $(".actions-dropdown").removeClass("hide");
      $(".no-data-txt").addClass("hide");
    }
  })
  
  //hover on table icons in Panel page
  $("#draft-table .icon-retrieve-events,#draft-table .icon-assign-access,#draft-table .icon-validate-connection").hover(function(){
      $(this).next().removeClass("hide");
    },function(){
      $(this).next().addClass("hide");
  })
  
  //click on table icons in Panel page
  $("#draft-table .icon-retrieve-events,#draft-table .icon-assign-access,#draft-table .icon-validate-connection").click(function(){
    if($(this).hasClass("icon-retrieve-events"))
    {
      clearTimeout(timer);
      $("#modal-retrieve-events .retrieve-events-content").removeClass("hide");
      $("#modal-retrieve-events .events-retrieved-successfully-content").addClass("hide");
      $("#modal-retrieve-events .lodding-bar .current-bar").stop();
      $("#modal-retrieve-events .lodding-bar .current-bar").css("width","0%");
      $("#modal-retrieve-events .lodding-bar .current-bar").animate({width:'100%'},4000);
      
      timer = setTimeout(function(){
        $("#modal-retrieve-events .retrieve-events-content").addClass("hide");
        $("#modal-retrieve-events .events-retrieved-successfully-content").removeClass("hide");
      },4000);
    }
    
    if($(this).hasClass("icon-validate-connection"))
    {
      clearTimeout(timer);
      $("#modal-validate-connection .retrieve-events-content").removeClass("hide");
      $("#modal-validate-connection .connection-validated-content").addClass("hide");
      $("#modal-validate-connection .lodding-bar .current-bar").stop();
      $("#modal-validate-connection .lodding-bar .current-bar").css("width","0%");
      $("#modal-validate-connection .lodding-bar .current-bar").animate({width:'100%'},4000);
      
      timer = setTimeout(function(){
        $("#modal-validate-connection .retrieve-events-content").addClass("hide");
        $("#modal-validate-connection .connection-validated-content").removeClass("hide");
      },4000);
    }
  })  
  
  //click on Panel rows
  $("#draft-table tr td.td-text").click(function(){
    if($(".integrate-events-contents").length>0)
    {
      return;
    }
    
    if($(this).parents("tr").hasClass("current-tr"))
    {
      return;
    }
    
    var clicked_row = $(this).parent();
    if(!clicked_row.hasClass("temp-row"))
    {
      $(".clicked-tr").removeClass("clicked-tr");
      
      if(!$(".right-panel-add").hasClass("hide") || !$(".right-panel-edit").hasClass("hide"))
      {
        //show Unsaved changes modal window
        $(".modal-unsaved-changes-trigger").click();
        clicked_row.addClass("clicked-tr");
      }
      else
      {
        //show Edit Panel screen          
        
        //show right side panel
        clicked_row.parents(".wrapper").find(".container-row").addClass("container-split");
        clicked_row.parents(".wrapper").find(".right-panel-edit").removeClass("hide");
        
        var panel_name_text = clicked_row.find("td").eq(1).html();
        var ip_address_text = clicked_row.find("td").eq(2).html();
        var dns_name_text = clicked_row.find("td").eq(3).find("span").html();
        
        clicked_row.parents(".wrapper").find(".right-panel-edit").find("h2").html(panel_name_text);
        clicked_row.parents(".wrapper").find(".right-panel-edit").find("input").eq(0).val(panel_name_text);
        clicked_row.parents(".wrapper").find(".right-panel-edit").find("input").eq(1).val(ip_address_text);
        clicked_row.parents(".wrapper").find(".right-panel-edit").find("input").eq(2).val(dns_name_text);
        
        clicked_row.parents(".wrapper").find(".right-panel-edit").find("input").prop("readonly","readonly");
        clicked_row.parents(".wrapper").find(".right-panel-edit").find("input").parent().addClass("no-border");
         
        clicked_row.parents(".wrapper").find(".right-panel-edit").find(".save-gray-btn").addClass("hide");
        clicked_row.parents(".wrapper").find(".right-panel-edit").find(".save-fail-btn").addClass("hide");
        clicked_row.parents(".wrapper").find(".right-panel-edit").find(".save-success-btn").addClass("hide");
        clicked_row.parents(".wrapper").find(".right-panel-edit").find(".cancel-btn").addClass("hide");
        clicked_row.parents(".wrapper").find(".right-panel-edit").find(".edit-btn").removeClass("hide");
        
        //hide Import button and Action dropdown
        $(".actions-dropdown").addClass("hide");
        $(".import-panel-btn").addClass("hide");
        
        //show new row in table
        clicked_row.addClass("current-tr");
      }
    }
  })
  
  //click Continue button in Unsaved Changes modal window
  $("#modal-unsaved-changes .continue-unsaved-changes-btn").click(function(){
    $(".right-panel-add .icon-cancel-btn").click();
    $(".right-panel-edit .icon-cancel-btn").click();
    
    $(".clicked-tr td.td-text").eq(0).click();
  })
  
  //click Save button first time in Edit Panel section
  $(".right-panel-edit .save-fail-btn").click(function(){
    $(".right-panel-edit .save-fail-btn").addClass("hide");
    $(".right-panel-edit .save-success-btn").removeClass("hide");
  })
  
  //click Save button second time in Edit Panel section
  $(".right-panel-edit .save-success-btn").click(function(){
    $(".right-panel-edit .save-fail-btn").removeClass("hide");
    $(".right-panel-edit .save-success-btn").addClass("hide");
  })
  
  //click Edit button second time in Edit Panel section
  $(".right-panel-edit .edit-btn").click(function(){
    $(".right-panel-edit .save-fail-btn").removeClass("hide");
    $(".right-panel-edit .save-success-btn").addClass("hide");
    $(".right-panel-edit .edit-btn").addClass("hide");
    $(".right-panel-edit .cancel-btn").removeClass("hide");
    
    $(".right-panel-edit").find("input").prop("readonly","");
    $(".right-panel-edit").find("input").parent().removeClass("no-border");
  })
  
  //click Cancel button second time in Edit Panel section
  $(".right-panel-edit .cancel-btn").click(function(){
    $(".right-panel-edit .save-fail-btn").addClass("hide");
    $(".right-panel-edit .save-success-btn").addClass("hide");
    $(".right-panel-edit .edit-btn").removeClass("hide");
    $(".right-panel-edit .cancel-btn").addClass("hide");
    
    $(".right-panel-edit").find("input").prop("readonly","readonly");
    $(".right-panel-edit").find("input").parent().addClass("no-border");
  })
  
  //click OK button in edit Panel modal window
  $("#modal-edit-panel-connection-successful .ok-edit-panel-btn").click(function(){
    $(".wrapper").find(".container-row").removeClass("container-split");
    $(".wrapper").find(".right-panel-add").addClass("hide");
    $(".wrapper").find(".right-panel-edit").addClass("hide");
    
    //show Import button and Action dropdown
    $(".actions-dropdown").removeClass("hide");
    $(".import-panel-btn").removeClass("hide");
    
    //update the new row
    var panel_name_text = $(".right-panel-edit").find(".panel-name-input").val();
    var ip_address_text = $(".right-panel-edit").find(".ip-address-input").val();
    var dns_name_text = $(".right-panel-edit").find(".dns-name-input").val();
    $("#draft-table .current-tr").find("td").eq(1).html(panel_name_text);
    $("#draft-table .current-tr").find("td").eq(2).html(ip_address_text);
    $("#draft-table .current-tr").find("td").eq(3).find("span").html(dns_name_text);
    $("#draft-table .current-tr").removeClass("current-tr");
  })
  
  //set the Current Date in Set Expiration step of modal window
  if($(".modal-set-expiration").find(".right-panel input.hasDatepicker").eq(0).length>0)
  {
    var current_day = new Date();
    var current_date_text = ((current_day.getMonth()+1)<10?"0":"")+(current_day.getMonth()+1)+"/"+((current_day.getDate())<10?"0":"")+current_day.getDate()+"/"+(current_day.getYear()+1900);
    $(".modal-set-expiration").find(".right-panel input.hasDatepicker").eq(0).val(current_date_text);
  }
  
  //click rows in Select Uer Badge table of Assign Access modal window
  $("#modal-assign-access .modal-select-user-badge-body .general-table tbody tr").click(function(){
    $(this).parents("#modal-assign-access").find(".modal-select-user-badge-body .container-row").addClass("container-split");
    $(this).parents("#modal-assign-access").find(".modal-select-user-badge-body .container-row").find(".right-panel").removeClass("hide");
    
    $(this).parents("#modal-assign-access").find(".modal-select-user-badge-body .general-table tr").removeClass("current-tr");
    $(this).addClass("current-tr");
  })
  
  //click rows in Select Uer Badge table of Assign Access modal window
  $("#modal-assign-access .modal-select-user-badge-body .general-table tbody tr .icon-right-arrow").click(function(event){
    if(!$(this).parent().parent().parent().hasClass("current-tr"))
      return;
      
    $(this).parent().parent().parent().removeClass("current-tr");
    $(this).parents(".container-row").removeClass("container-split");
    $(this).parents(".container-row").find(".right-panel").addClass("hide");
    
    event.stopPropagation();
  })
  
  //click rows in Badges section of Select Uer Badge table of Assign Access modal window
  $("#modal-assign-access .modal-select-user-badge-body .right-panel-content .data-list ul li").click(function(){
    $(this).parent().find("li").removeClass("current-tr");
    $(this).addClass("current-tr");
    
    $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(0) .next-btn").removeClass("btn-gray").addClass("btn-blue");
  })
  
  //click Up/Down arrows to collapse/expand the content in Assign Access modal window and Assign Access/Change Access pages
  $("#modal-assign-access .show-row-btn,#modal-assign-access .hide-row-btn,\
     .assign-access-contents .right-panel .show-row-btn,.assign-access-contents .right-panel .hide-row-btn,\
     .change-access-contents .right-panel .show-row-btn,.change-access-contents .right-panel .hide-row-btn,\
     .integrate-events-contents .right-panel .show-row-btn,.integrate-events-contents .right-panel .hide-row-btn").click(function(){
    if($(this).hasClass("show-row-btn"))
    {
      $(this).removeClass("show-row-btn").addClass("hide-row-btn");
      $(this).parent().removeClass("current-bg");
      $(this).next().addClass("hide");
    }
    else
    {
      $(this).parents(".right-panel").find(".show-row-btn").click();
      
      $(this).removeClass("hide-row-btn").addClass("show-row-btn");
      $(this).parent().addClass("current-bg");
      $(this).next().removeClass("hide");
    }
  })
  
  //click checkboxes Table header rows in Select Access Levels table of Assign Access modal window in Panel List page
  $("#modal-assign-access .modal-select-access-level-body .first-level-titles .jqTransformCheckbox").click(function(event){
    if($(this).hasClass("jqTransformChecked"))
    {
      //check all check box
      $(this).parents(".modal-select-access-level-body").find(".one-area a.jqTransformCheckbox").addClass("jqTransformChecked");
      $(this).parents(".modal-select-access-level-body").find(".one-area a.jqTransformCheckbox").next().prop("checked","checked");
    }
    else
    {
      //uncheck all checkbox
      $(this).parents(".modal-select-access-level-body").find(".one-area a.jqTransformCheckbox").removeClass("jqTransformChecked");
      $(this).parents(".modal-select-access-level-body").find(".one-area a.jqTransformCheckbox").next().prop("checked",false);
    }
    
    if($(this).parents(".modal-select-access-level-body").find(".one-area .jqTransformCheckbox.jqTransformChecked").length>0)
    {
      $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(1) .next-btn").removeClass("btn-gray").addClass("btn-blue");
    }
    else
    {
      $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(1) .next-btn").removeClass("btn-blue").addClass("btn-gray");
    }
    
    event.stopPropagation();
  })
  
  //click checkboxes Table rows in Select Access Levels table of Assign Access modal window in Panel List page
  $("#modal-assign-access .modal-select-access-level-body .one-area .jqTransformCheckbox").click(function(event){
    if($(this).parents(".modal-select-access-level-body").find(".one-area .jqTransformCheckbox.jqTransformChecked").length>0)
    {
      $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(1) .next-btn").removeClass("btn-gray").addClass("btn-blue");
    }
    else
    {
      $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(1) .next-btn").removeClass("btn-blue").addClass("btn-gray");
    }
    
    var table_rows = $(this).parents(".modal-select-access-level-body").find(".one-area");
    if(table_rows.find("a.jqTransformCheckbox").length === table_rows.find("a.jqTransformCheckbox.jqTransformChecked").length)
    {
      //check check all checkbox
      $(this).parents(".modal-select-access-level-body").find(".first-level-titles a.jqTransformCheckbox").addClass("jqTransformChecked");
      $(this).parents(".modal-select-access-level-body").find(".first-level-titles a.jqTransformCheckbox").next().prop("checked","checked");
    }
    else
    {
      //uncheck check all checkbox
      $(this).parents(".modal-select-access-level-body").find(".first-level-titles a.jqTransformCheckbox").removeClass("jqTransformChecked");
      $(this).parents(".modal-select-access-level-body").find(".first-level-titles a.jqTransformCheckbox").next().prop("checked",false);
    }
    
    event.stopPropagation();
  })
  $("#modal-assign-access .modal-select-access-level-body .one-area .jqTransformCheckboxWrapper").click(function(event){
    event.stopPropagation();
  })
  
  //click First level table rows to collapse/expand the content of Select Access Levels table of Assign Access modal window
  $("#modal-assign-access .modal-select-access-level-body .one-area").click(function(){
    if(!$(this).parent().hasClass("show-box-row"))
    {
      $(this).parent().siblings(".show-box-row").find(".one-area").click();
    }
  
    $(this).parent().toggleClass("show-box-row");
    
    $(this).parents("#modal-assign-access").find(".current-row").removeClass("current-row");
    if($(this).parent().hasClass("show-box-row"))
    {
      $(this).parent().addClass("current-row");
    }
  })
  
  //click Second level table rows to collapse/expand the content of Select Access Levels table of Assign Access modal window
  $("#modal-assign-access .modal-select-access-level-body .two-area").click(function(){
    if(!$(this).parent().hasClass("show-box-row"))
    {
      $(this).parent().siblings(".show-box-row").find(".two-area").eq(0).click();
    }
    
    $(this).parent().toggleClass("show-box-row");
  })
  
  //click tabs in Select Access Levels table of Assign Access modal window
  $("#modal-assign-access .modal-select-access-level-body .nav-tab li a").click(function(){
    var tab_contents = $(this).parent().parent().parent().siblings(".tab-contents");
    
    var index = $(this).parent().parent().find("li a").index($(this));
    
    $(this).parent().parent().find("li").removeClass("current");
    $(this).parent().addClass("current");
    
    tab_contents.addClass("hide");
    tab_contents.eq(index).removeClass("hide");
  })
  
  //click checkbox of Table header in Set Expiration table of Assign Access modal window in Panel List/Assign Access page
  $("#modal-assign-access .modal-set-expiration thead tr .jqTransformCheckbox").click(function(event){
    if($(this).hasClass("jqTransformChecked"))
    {
      //check all check box
      $(this).parents(".modal-set-expiration").find("tbody tr a.jqTransformCheckbox").addClass("jqTransformChecked");
      $(this).parents(".modal-set-expiration").find("tbody tr a.jqTransformCheckbox").next().prop("checked","checked");
      
      $(this).parents(".modal-set-expiration").find("tbody tr").addClass("current-tr");
      $(this).parents(".modal-set-expiration").find(".right-panel").removeClass("hide");
      $(this).parents(".modal-set-expiration").find(".container-row").addClass("expiration-container-split");
    }
    else
    {
      //uncheck all checkbox
      $(this).parents(".modal-set-expiration").find("tbody tr a.jqTransformCheckbox").removeClass("jqTransformChecked");
      $(this).parents(".modal-set-expiration").find("tbody tr a.jqTransformCheckbox").next().prop("checked",false);
      
      $(this).parents(".modal-set-expiration").find("tbody tr").removeClass("current-tr");
      $(this).parents(".modal-set-expiration").find(".right-panel").addClass("hide");
      $(this).parents(".modal-set-expiration").find(".container-row").removeClass("expiration-container-split");
    }
  })
  
  //click checkbox of Table rows in Set Expiration table of Assign Access modal window in Panel List/Assign Access page
  $("#modal-assign-access .modal-set-expiration tbody tr .jqTransformCheckbox").click(function(){
    if($(this).hasClass("jqTransformChecked"))
    {
      $(this).parents("tr").addClass("current-tr");
    }
    else
    {
      $(this).parents("tr").removeClass("current-tr");
    }
    
    var table_rows = $(this).parents(".modal-set-expiration").find("tbody tr");
    if(table_rows.find("a.jqTransformCheckbox").length === table_rows.find("a.jqTransformCheckbox.jqTransformChecked").length)
    {
      //check check all checkbox
      $(this).parents(".modal-set-expiration").find("thead tr a.jqTransformCheckbox").addClass("jqTransformChecked");
      $(this).parents(".modal-set-expiration").find("thead tr a.jqTransformCheckbox").next().prop("checked","checked");
    }
    else
    {
      //uncheck check all checkbox
      $(this).parents(".modal-set-expiration").find("thead tr a.jqTransformCheckbox").removeClass("jqTransformChecked");
      $(this).parents(".modal-set-expiration").find("thead tr a.jqTransformCheckbox").next().prop("checked",false);
    }
    
    if(table_rows.find("a.jqTransformCheckbox.jqTransformChecked").length === 0)
    {
      $(this).parents(".modal-set-expiration").find(".right-panel").addClass("hide");
      $(this).parents(".modal-set-expiration").find(".container-row").removeClass("expiration-container-split");
    }
    else
    {
      $(this).parents(".modal-set-expiration").find(".right-panel").removeClass("hide");
      $(this).parents(".modal-set-expiration").find(".container-row").addClass("expiration-container-split");
    }
  })
  
  //choose dates in Set Expiration table of Assign Access modal window in Panel List page
  $("#modal-assign-access .modal-set-expiration .right-panel  input.hasDatepicker").change(function(){
    var pass = true;
    
    var data_input = $(this).parent().parent().find("input.hasDatepicker");
    for(var i=0;i<data_input.length;i++)
    {
      if(data_input.eq(i).val() === "")
      {
        pass = false;
      }
    }
        
    if(pass)
    {
      if($(".panel-contents").length>0)
      {
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns").eq(2).find(".assign-btn-gray").addClass("hide");
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns").eq(2).find(".assign-btn-fail").removeClass("hide");
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns").eq(2).find(".assign-btn-success").addClass("hide");
      }
      
      if($(".assign-access-contents").length>0)
      {
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns").eq(1).find(".assign-btn-gray").addClass("hide");
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns").eq(1).find(".assign-btn-fail").removeClass("hide");
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns").eq(1).find(".assign-btn-success").addClass("hide");
      }
    }
    else
    {
      if($(".panel-contents").length>0)
      {
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns").eq(2).find(".assign-btn-gray").removeClass("hide");
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns").eq(2).find(".assign-btn-fail").addClass("hide");
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns").eq(2).find(".assign-btn-success").addClass("hide");
      }
      
      if($(".assign-access-contents").length>0)
      {
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns").eq(1).find(".assign-btn-gray").removeClass("hide");
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns").eq(1).find(".assign-btn-fail").addClass("hide");
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns").eq(1).find(".assign-btn-success").addClass("hide");
      }
    }
  })
  
  //first time click on Assign button in Assign Access modal window in Panel List page
  $("#modal-assign-access .modal-bottom-btn .step-btns .assign-btn-fail").click(function(){
    $("#modal-assign-access .modal-bottom-btn .step-btns .assign-btn-fail").addClass("hide");
    $("#modal-assign-access .modal-bottom-btn .step-btns .assign-btn-success").removeClass("hide");
  })
  
  //first time click on Assign button in Assign Access modal window in Panel List page
  $("#modal-assign-access .modal-bottom-btn .step-btns .assign-btn-success").click(function(){
    $("#modal-assign-access .modal-bottom-btn .step-btns .assign-btn-fail").removeClass("hide");
    $("#modal-assign-access .modal-bottom-btn .step-btns .assign-btn-success").addClass("hide");
  })
  
  //click Previous/Next button in modal window
  $("#modal-assign-access .modal-bottom-btn .previous-btn,#modal-assign-access .modal-bottom-btn .next-btn").click(function(){
    if($(this).hasClass("btn-gray"))
      return;
    
    var step_contents = $(this).parents("#modal-assign-access").find(".step-content");
    var current_step_index = 0;
    for(var i=0;i<step_contents.length;i++)
    {
      if(!step_contents.eq(i).hasClass("hide"))
      {
        current_step_index = i;
      }
    }
    
    var new_step_index;
    if($(this).hasClass("next-btn"))
    {
      new_step_index = current_step_index+1;
    }
    else if($(this).hasClass("previous-btn"))
    {
      new_step_index = current_step_index-1;
    }
    step_contents.eq(current_step_index).addClass("hide");
    step_contents.eq(new_step_index).removeClass("hide");
    
    if($(this).hasClass("next-btn"))
    {
      $(this).parents("#modal-assign-access").find(".step-nav").find("li.already").removeClass("already");
      $(this).parents("#modal-assign-access").find(".step-nav").find("li.current").removeClass("current").addClass("already");
      $(this).parents("#modal-assign-access").find(".step-nav").find("li").eq(new_step_index).addClass("current");
      $(this).parents("#modal-assign-access").find(".step-nav").find("li").eq(new_step_index).addClass("completed");
    }
    else if($(this).hasClass("previous-btn"))
    {
      $(this).parents("#modal-assign-access").find(".step-nav").find("li.already").removeClass("already");
      $(this).parents("#modal-assign-access").find(".step-nav").find("li.current").removeClass("current");
      $(this).parents("#modal-assign-access").find(".step-nav").find("li").eq(new_step_index).addClass("current");
      if(new_step_index>0)
      {
        $(this).parents("#modal-assign-access").find(".step-nav").find("li").eq(new_step_index-1).addClass("already");
      }
    }
    
    $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns").addClass("hide");
    $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns").eq(new_step_index).removeClass("hide");
  })
  
  //click Steps button in modal window
  $("#modal-assign-access .step-nav li a").click(function(){
    if((!$(this).parent().hasClass("completed")) && (!$(this).parent().hasClass("current")))
      return;
    
    var step_contents = $(this).parents("#modal-assign-access").find(".step-content");
    var clicked_step_index = $(this).parent().parent().find("li a").index($(this));
    
    $(this).parents("#modal-assign-access").find(".step-nav").find("li.already").removeClass("already");
    $(this).parents("#modal-assign-access").find(".step-nav").find("li.current").removeClass("current");
    $(this).parents("#modal-assign-access").find(".step-nav").find("li").eq(clicked_step_index).addClass("current");
    if(clicked_step_index>0)
    {
      $(this).parents("#modal-assign-access").find(".step-nav").find("li").eq(clicked_step_index-1).addClass("already");
    }
    step_contents.addClass("hide");
    step_contents.eq(clicked_step_index).removeClass("hide");
   
    $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns").addClass("hide");
    $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns").eq(clicked_step_index).removeClass("hide");
  })
  
  //click on table rows in Assign Access table of Assign Access/Change Access page
  $(".assign-access-contents #draft-table tbody tr,\
     .change-access-contents #draft-table tbody tr,\
     .integrate-events-contents #draft-table tbody tr").click(function(){
    if($(this).hasClass("current-tr"))
    {
      return;
    }
    if($(this).hasClass("disabled-row"))
    {
      return;
    }
    
    if(($(".integrate-events-contents").length>0) && (!$(".right-panel-edit").hasClass("hide")))
    {
      //show Unsaved changes modal window
      $(".modal-unsaved-changes-trigger").click();
      $(".integrate-events-contents #draft-table tr").removeClass("clicked-tr");
      $(this).addClass("clicked-tr");
    }
    else
    {
      $(this).parent().find("tr").removeClass("current-tr");
      $(this).addClass("current-tr");
   
      $(this).parents(".contents").find(".right-panel-edit").removeClass("hide");
      $(this).parents(".contents").find(".container-row").addClass("container-split");
    
      $(this).parents(".contents").find(".actions-dropdown").addClass("hide");
    }
  })
  
  //click on Selected Table row to diselect row in Panel List/Assign Access/Change Access/Integrate Events pages
  $("#draft-table tr .icon-right-arrow").click(function(event){
    if(!$(this).parent().parent().parent().hasClass("current-tr"))
      return;
      
    $(this).parent().parent().parent().removeClass("current-tr");
    $(this).parents(".container-row").removeClass("container-split");
    $(this).parents(".container-row").find(".right-panel-edit").addClass("hide");
    
    event.stopPropagation();
  })
  
  //click rows in Badges section in Assign Access table of Assign Access page
  $(".assign-access-contents .right-panel .data-list ul li,.change-access-contents .right-panel .data-list ul li").click(function(){
    $(this).parent().find("li").removeClass("current-tr");
    $(this).addClass("current-tr");
    
    $(this).parents(".right-panel").find(".btn-assign-access .btn-gray").addClass("hide");
    $(this).parents(".right-panel").find(".btn-assign-access .btn-blue").removeClass("hide");
  })
  
  //click checkboxes of Table Header rows in Select Access Levels table of Assign Access modal window in Assign Access/Change Access page
  $("#modal-assign-access .modal-select-access-level-body .tab-readers-contents .second-level-titles .jqTransformCheckbox").click(function(event){
    if($(this).hasClass("jqTransformChecked"))
    {
      //check all check box
      $(this).parents(".tab-readers-contents").find(".sub-box-row a.jqTransformCheckbox").addClass("jqTransformChecked");
      $(this).parents(".tab-readers-contents").find(".sub-box-row a.jqTransformCheckbox").next().prop("checked","checked");
    }
    else
    {
      //uncheck all checkbox
      $(this).parents(".tab-readers-contents").find(".sub-box-row a.jqTransformCheckbox").removeClass("jqTransformChecked");
      $(this).parents(".tab-readers-contents").find(".sub-box-row a.jqTransformCheckbox").next().prop("checked",false);
    }
    
    if($(this).parents(".modal-select-access-level-body").find(".two-area .jqTransformCheckbox.jqTransformChecked").length>0)
    {
      if($(".change-access-contents").length === 0)
      {
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(0) .next-btn").removeClass("btn-gray").addClass("btn-blue");
      }
      else
      {
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(0) .next-btn-fail").removeClass("btn-gray").addClass("btn-blue").removeClass("hide");
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(0) .next-btn").removeClass("btn-gray").addClass("btn-blue").addClass("hide");
      }
    }
    else
    {
      if($(".change-access-contents").length === 0)
      {
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(0) .next-btn").removeClass("btn-blue").addClass("btn-gray");
      }
      else
      {
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(0) .next-btn-fail").removeClass("btn-blue").addClass("btn-gray").addClass("hide");
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(0) .next-btn").removeClass("btn-blue").addClass("btn-gray").removeClass("hide");
      }
    }
  })
  //click checkboxes of Table rows in Select Access Levels table of Assign Access modal window in Assign Access/Change Access page
  $("#modal-assign-access .modal-select-access-level-body .two-area .jqTransformCheckbox").click(function(event){
    if($(this).parents(".modal-select-access-level-body").find(".two-area .jqTransformCheckbox.jqTransformChecked").length>0)
    {
      if($(".change-access-contents").length === 0)
      {
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(0) .next-btn").removeClass("btn-gray").addClass("btn-blue");
      }
      else
      {
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(0) .next-btn-fail").removeClass("btn-gray").addClass("btn-blue").removeClass("hide");
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(0) .next-btn").removeClass("btn-gray").addClass("btn-blue").addClass("hide");
      }
    }
    else
    {
      if($(".change-access-contents").length === 0)
      {
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(0) .next-btn").removeClass("btn-blue").addClass("btn-gray");
      }
      else
      {
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(0) .next-btn-fail").removeClass("btn-blue").addClass("btn-gray").addClass("hide");
        $(this).parents("#modal-assign-access").find(".modal-bottom-btn .step-btns:eq(0) .next-btn").removeClass("btn-blue").addClass("btn-gray").removeClass("hide");
      }
    }
    
    var table_rows = $(this).parents(".tab-readers-contents").find(".sub-box-row");
    if(table_rows.find("a.jqTransformCheckbox").length === table_rows.find("a.jqTransformCheckbox.jqTransformChecked").length)
    {
      //check check all checkbox
      $(this).parents(".tab-readers-contents").find(".second-level-titles a.jqTransformCheckbox").addClass("jqTransformChecked");
      $(this).parents(".tab-readers-contents").find(".second-level-titles a.jqTransformCheckbox").next().prop("checked","checked");
    }
    else
    {
      //uncheck check all checkbox
      $(this).parents(".tab-readers-contents").find(".second-level-titles a.jqTransformCheckbox").removeClass("jqTransformChecked");
      $(this).parents(".tab-readers-contents").find(".second-level-titles a.jqTransformCheckbox").next().prop("checked",false);
    }
    
    event.stopPropagation();
  })
  $("#modal-assign-access .modal-select-access-level-body .two-area .jqTransformCheckboxWrapper").click(function(event){
    event.stopPropagation();
  })
  
  //click First time on Next button in Change Access modal window
  $("#modal-assign-access .next-btn-fail").click(function(){
    if($(this).hasClass("btn-gray"))
    {
      return;
    }
    
    $(this).parent().find(".next-btn-fail").addClass("hide");
    $(this).parent().find(".next-btn-success").removeClass("hide");
  })
  
  //click Second time on Next button in Change Access modal window
  $("#modal-assign-access .next-btn-success").click(function(){
    if($(this).hasClass("btn-gray"))
    {
      return;
    }
    
    $(this).parent().find(".next-btn-fail").removeClass("hide");
    $(this).parent().find(".next-btn-success").addClass("hide");
  })
  
  //click checkbox of Table header in Panel List page and Integrate Events page
  $(".panel-contents #draft-table thead tr .jqTransformCheckboxWrapper .jqTransformCheckbox,\
     .integrate-events-contents #draft-table thead tr .jqTransformCheckboxWrapper .jqTransformCheckbox").click(function(event){
    $(this).toggleClass("jqTransformChecked");
    
    if($(this).hasClass("jqTransformChecked"))
    {
      //check all check box
      $(this).parents("#draft-table").find("tbody tr a.jqTransformCheckbox").addClass("jqTransformChecked");
      $(this).parents("#draft-table").find("tbody tr a.jqTransformCheckbox").next().prop("checked","checked");
      
      $(this).parents("#draft-table").find("tbody tr").addClass("yellow-tr");
      $(this).parents("#draft-table").find("tbody tr.disabled-row").removeClass("yellow-tr");
    }
    else
    {
      //uncheck all checkbox
      $(this).parents("#draft-table").find("tbody tr a.jqTransformCheckbox").removeClass("jqTransformChecked");
      $(this).parents("#draft-table").find("tbody tr a.jqTransformCheckbox").next().prop("checked",false);
      $(this).parents("#draft-table").find("tbody tr").removeClass("yellow-tr");
    }
  })
  
  //click checkbox of Table rows in Panel List page and Integrate Events page
  $(".panel-contents #draft-table tr .jqTransformCheckboxWrapper .jqTransformCheckbox,\
    .integrate-events-contents #draft-table tr .jqTransformCheckboxWrapper .jqTransformCheckbox").click(function(){
    if($(this).hasClass("jqTransformChecked"))
    {
      if(!$(this).parents("#draft-table tr").hasClass("disabled-row"))
        $(this).parents("#draft-table tr").addClass("yellow-tr");
      else
        $(this).parents("#draft-table tr").removeClass("yellow-tr");
    }
    else
    {
      $(this).parents("#draft-table tr").removeClass("yellow-tr");
    }
    
    var table_rows = $(this).parents("#draft-table").find("tbody tr");
    if(table_rows.find("a.jqTransformCheckbox").length === table_rows.find("a.jqTransformCheckbox.jqTransformChecked").length)
    {
      //check check all checkbox
      $(this).parents("#draft-table").find("thead tr a.jqTransformCheckbox").addClass("jqTransformChecked");
      $(this).parents("#draft-table").find("thead tr a.jqTransformCheckbox").next().prop("checked","checked");
    }
    else
    {
      //uncheck check all checkbox
      $(this).parents("#draft-table").find("thead tr a.jqTransformCheckbox").removeClass("jqTransformChecked");
      $(this).parents("#draft-table").find("thead tr a.jqTransformCheckbox").next().prop("checked",false);
    }
  })
  $(".panel-contents #draft-table tr .jqTransformCheckboxWrapper,\
     .integrate-events-contents #draft-table tr .jqTransformCheckboxWrapper").click(function(event){
    event.stopPropagation();
  })
  
  //click Enable link in Integrate Events page
  $(".modal-enable-btn").click(function(){
    var table_rows = $(".integrate-events-contents #draft-table tbody tr");
    for(var i=0;i<table_rows.length;i++)
    {
      if(table_rows.eq(i).find(".jqTransformCheckbox").hasClass("jqTransformChecked"))
      {
        table_rows.eq(i).removeClass("disabled-row").addClass("yellow-tr");;
        table_rows.eq(i).find("td:eq(4)").html("Enabled");
      }
    }
  })
  
  //click Disable button of Disable modal in Integrate Events page
  $(".modal-disable-btn").click(function(){
    var table_rows = $(".integrate-events-contents #draft-table tbody tr");
    for(var i=0;i<table_rows.length;i++)
    {
      if(table_rows.eq(i).find(".jqTransformCheckbox").hasClass("jqTransformChecked"))
      {
        table_rows.eq(i).addClass("disabled-row").removeClass("yellow-tr");
        table_rows.eq(i).find("td:eq(4)").html("Disabled");
      }
    }
  })
  
  //click Save link of Set Severity modal window in Integrate Events page
  $(".modal-save-severity-btn").click(function(){
    var new_value = $(this).parents("#modal-set-severity").find(".actions-dropdown .selected-option").html();
    var table_rows = $(".integrate-events-contents #draft-table tbody tr");
    for(var i=0;i<table_rows.length;i++)
    {
      if(table_rows.eq(i).find(".jqTransformCheckbox").hasClass("jqTransformChecked"))
      {
        table_rows.eq(i).find("td:eq(3)").html(new_value);
      }
    }
  })
  
  //click Table rows of right side panel in Integrate Events page
  $(".integrate-events-contents .panel-table-form tbody tr").click(function(){
    $(this).parent().find("tr").removeClass("current-tr");
    $(this).addClass("current-tr");
  })
});